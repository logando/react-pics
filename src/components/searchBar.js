import React from 'react';

class searchBar extends React.Component {
    state = { term: '' };

    onFormSumit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state.term);
    };

    render() {
        return (
            <div className="ui segment">
                <form onSubmit={this.onFormSumit} className="ui form">
                    <div className="field">
                        <label>Image Search</label>
                        <input type='text'
                            value={this.state.term}
                            onChange={e => { this.setState({ term: e.target.value }) }}></input>
                    </div>
                </form>
            </div>
        );
    }
}

export default searchBar;