import axios from 'axios';

export default axios.create({
    baseURL:'https://api.unsplash.com',
    headers:{
        Authorization: 'Client-ID 702f6a78fca26baa6138542c763884c13e944575288d55263e1a6566e95baffa'
    }
});